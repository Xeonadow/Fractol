/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 19:59:08 by lpousse           #+#    #+#             */
/*   Updated: 2016/12/07 22:38:34 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		shade(unsigned int col1, unsigned int col2,
					double ratio, unsigned int *color)
{
	unsigned int	sub1;
	unsigned int	sub2;
	unsigned int	diff;
	unsigned int	offset;
	unsigned int	mask;

	if (col1 == col2)
		return ;
	offset = 0;
	mask = 0xFF;
	while (offset < 32)
	{
		sub2 = col2 & mask;
		sub1 = col1 & mask;
		if (sub2 < sub1)
			diff = ((sub1 - sub2) >> offset) * ratio;
		else
			diff = ((sub2 - sub1) >> offset) * ratio;
		*color += (sub2 < sub1 ? -1 : 1) * diff * (mask / 0xFF);
		mask *= 0x100;
		offset += 8;
	}
}

void		colors(t_env *env)
{
	double	ratio;
	int		i;

	if (env->colors != NULL)
		free(env->colors);
	env->colors = (unsigned int *)malloc(sizeof(unsigned int)
											* (env->iter - 1));
	if (env->colors == NULL)
		error(errno);
	i = 0;
	while (i < env->iter - 1)
	{
		ratio = (double)(i) / (double)(env->iter - 1);
		env->colors[i] = env->col1;
		shade(env->col1, env->col2, ratio, &(env->colors[i]));
		i++;
	}
}

static int	iteration(t_env *env, int iter, int (*f)(int, int, int, t_env *))
{
	int		i;
	int		j;
	int		tmp;

	i = 0;
	while (i < env->winy)
	{
		j = -1;
		while (j < env->winx)
		{
			tmp = f(i, j, iter, env);
			if (tmp < iter)
				put_pix_img(&(env->imgdata), j, i,
						mlx_get_color_value(env->mlx, env->colors[tmp - 1]));
			else
				put_pix_img(&(env->imgdata), j, i,
						mlx_get_color_value(env->mlx, env->bkg));
			j++;
		}
		i++;
	}
	return (0);
}

int			draw(t_env *env)
{
	int		(*f)(int, int, int, t_env *);

	clear_img(&(env->imgdata));
	f = NULL;
	if (env->fractal == 1)
		f = mandelbrot;
	else if (env->fractal == 2)
		f = julia;
	else if (env->fractal == 3)
		f = burning_ship;
	else if (env->fractal == 4)
		f = burning_julia;
	iteration(env, env->iter, f);
	expose_hook(env);
	return (0);
}
