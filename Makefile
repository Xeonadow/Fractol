# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/26 12:50:44 by lpousse           #+#    #+#              #
#    Updated: 2016/12/07 21:39:25 by lpousse          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol
SRC_NAME = fractol.c hooks.c put_pixel_to_image.c drawing.c controls.c fractals.c
SRC_PATH = ./
OBJ_PATH = obj/
INC_PATH = ./ libft/includes/

CC = gcc
CFLAGS = -Wall -Wextra -Werror
CPPFLAGS = $(addprefix -I ,$(INC_PATH))
LDFLAGS = -L libft -L/usr/local/lib
LDLIBS = -lft -lmlx -framework OpenGL -framework AppKit

OBJ_NAME = $(SRC_NAME:.c=.o)
SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))

all: mklib $(NAME)

$(NAME): $(OBJ)
	$(CC) $(LDFLAGS) $(LDLIBS) $^ -o $@

mklib:
	make -C libft -j8

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(addsuffix *.h,$(INC_PATH))
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

clean:
	rm -fv $(OBJ)
	@rmdir $(OBJ_PATH) 2> /dev/null || true
	make -C libft clean

fclean: clean
	rm -fv $(NAME)
	make -C libft fclean

wclean:
	rm $(SRC_PATH)*~ $(addsuffix *~,$(INC_PATH))

re: fclean all

norme:
	norminette $(SRC) ./*.h

.PHONY: all clean fclean re mklib norme
