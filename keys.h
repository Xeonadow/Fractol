/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 19:49:16 by lpousse           #+#    #+#             */
/*   Updated: 2016/12/07 19:11:41 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYS_H
# define KEYS_H

# define KBD_ESC 53
# define KBD_TAB 48
# define KBD_1 18
# define KBD_2 19
# define KBD_3 20
# define KBD_4 21
# define KBD_5 23
# define KBD_6 22
# define KBD_SPACE 49
# define KBD_UP 126
# define KBD_DOWN 125
# define KBD_LEFT 123
# define KBD_RIGHT 124
# define KBD_W 13
# define KBD_S 1
# define KBD_A 0
# define KBD_D 2
# define KBD_Q 12
# define KBD_E 14
# define PAD_PLUS 69
# define PAD_MINUS 78
# define PAD_STAR 67
# define PAD_SLASH 75
# define PAD_EQUAL 81
# define PAD_DOT 65
# define PAD_0 82
# define PAD_1 83
# define PAD_2 84
# define PAD_3 85
# define PAD_4 86
# define PAD_5 87
# define PAD_6 88
# define PAD_7 89
# define PAD_8 91
# define PAD_9 92
# define MOUSE_LEFT 1
# define MOUSE_RIGHT 2
# define WHEEL_UP 4
# define WHEEL_DOWN 5
# define WHEEL_LEFT 6
# define WHEEL_RIGHT 7

#endif
