/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_pixel_to_image.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/02 15:58:46 by lpousse           #+#    #+#             */
/*   Updated: 2016/11/29 19:51:25 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		init_imgdata(void *img, t_env *env)
{
	t_imgdata	*imgdata;

	imgdata = &(env->imgdata);
	imgdata->img = mlx_get_data_addr(img, &(imgdata->bppixel),
							&(imgdata->size_line), &(imgdata->endian));
	imgdata->x = env->winx;
	imgdata->y = env->winy;
}

void		clear_img(t_imgdata *img)
{
	ft_bzero(img->img, img->size_line * img->y);
}

void		put_pix_img(t_imgdata *img, int x, int y, unsigned int color)
{
	int		i;
	int		j;
	int		bytespp;

	if (x < 0 || x >= img->x || y < 0 || y >= img->y)
		return ;
	bytespp = img->bppixel / 8 + (img->bppixel % 8 != 0);
	i = y * img->size_line + x * bytespp;
	j = 0;
	while (j < bytespp)
	{
		if (img->endian == 1)
			(img->img)[i + (bytespp - j - 1)] = (color >> (j * 8)) & 0xff;
		else
			(img->img)[i + j] = (color >> (j * 8)) & 0xff;
		j++;
	}
}
