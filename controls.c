/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controls.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 19:22:20 by lpousse           #+#    #+#             */
/*   Updated: 2016/12/07 22:38:54 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	iteration_control(int key, t_env *env)
{
	if (key == PAD_STAR)
		env->iter += ITER;
	else if (env->iter - ITER >= 0)
		env->iter -= ITER;
	else
		env->iter = 0;
	if (env->iter != 0)
		colors(env);
	draw(env);
}

void	move_control(int key, t_env *env)
{
	if (key == KBD_UP)
		env->y -= MOVE;
	else if (key == KBD_DOWN)
		env->y += MOVE;
	else if (key == KBD_LEFT)
		env->x -= MOVE;
	else if (key == KBD_RIGHT)
		env->x += MOVE;
	draw(env);
}

void	fractal_control(int key, t_env *env)
{
	int		tmp;

	tmp = 1;
	if (key == KBD_SPACE && (env->fractal == 2 || env->fractal == 4))
	{
		env->pause = 1 - env->pause;
		return ;
	}
	if (key == KBD_1)
		tmp = 1;
	else if (key == KBD_2)
		tmp = 2;
	else if (key == KBD_3)
		tmp = 3;
	else if (key == KBD_4)
		tmp = 4;
	if (tmp != env->fractal)
	{
		env->pause = 0;
		env->fractal = tmp;
		draw(env);
	}
}

void	zoom_control(int key, t_env *env)
{
	env->zoom *= key == PAD_MINUS ? ZOOM : 1 / ZOOM;
	env->x *= key == PAD_PLUS ? ZOOM : 1 / ZOOM;
	env->y *= key == PAD_PLUS ? ZOOM : 1 / ZOOM;
	draw(env);
}

void	color_control(int key, t_env *env)
{
	static unsigned int	tab[10] = {0xFFFFFF, 0xFF0000, 0xFF7F00, 0xFFFF00,
									0x00FF00, 0x00FFFF, 0x0000FF, 0x8B00FF,
									0x303030, 0x000000};
	unsigned int		*tmp;
	int					add;
	int					i;

	add = 1;
	if (key == PAD_4 || key == PAD_5 || key == PAD_6)
		add = -1;
	if (key == PAD_1 || key == PAD_4)
		tmp = &(env->col1);
	else if (key == PAD_2 || key == PAD_5)
		tmp = &(env->col2);
	else
		tmp = &(env->bkg);
	i = 0;
	while (tab[i] != *tmp)
		i++;
	i = (i + add) % 10;
	*tmp = tab[i < 0 ? 10 + i : i];
	colors(env);
	draw(env);
}
