/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractals.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 20:59:12 by lpousse           #+#    #+#             */
/*   Updated: 2016/12/07 21:38:07 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		mandelbrot(int i, int j, int iter, t_env *env)
{
	double	tmp;
	double	x;
	double	y;
	double	rec;
	double	imc;

	x = 0;
	y = 0;
	rec = env->zoom * ((j - env->winx / 2 + env->x) * 4 / env->winx);
	imc = env->zoom * ((i - env->winy / 2 + env->y) * 4 / env->winx);
	while (iter && x * x + y * y < 4)
	{
		tmp = x * x - y * y + rec;
		y = 2 * x * y + imc;
		x = tmp;
		iter--;
	}
	return (env->iter - iter);
}

int		julia(int i, int j, int iter, t_env *env)
{
	double	tmp;
	double	x;
	double	y;

	x = env->zoom * ((j - env->winx / 2 + env->x) * 4 / env->winx);
	y = env->zoom * ((i - env->winy / 2 + env->y) * 4 / env->winx);
	iter--;
	while (x * x + y * y < 4 && iter)
	{
		tmp = x * x - y * y + env->juliax;
		y = 2 * x * y + env->juliay;
		x = tmp;
		iter--;
	}
	return (env->iter - iter);
}

int		burning_ship(int i, int j, int iter, t_env *env)
{
	double	tmp;
	double	x;
	double	y;
	double	rec;
	double	imc;

	x = 0;
	y = 0;
	rec = env->zoom * ((j - env->winx / 2 + env->x) * 4 / env->winx);
	imc = env->zoom * ((i - env->winy / 2 + env->y) * 4 / env->winx);
	while (x * x + y * y < 4 && iter)
	{
		tmp = fabs(x) * fabs(x) - fabs(y) * fabs(y) + rec;
		y = 2 * fabs(x) * fabs(y) + imc;
		x = tmp;
		iter--;
	}
	return (env->iter - iter);
}

int		burning_julia(int i, int j, int iter, t_env *env)
{
	double	tmp;
	double	x;
	double	y;

	x = env->zoom * ((j - env->winx / 2 + env->x) * 4 / env->winx);
	y = env->zoom * ((i - env->winy / 2 + env->y) * 4 / env->winx);
	iter--;
	while (x * x + y * y < 4 && iter)
	{
		tmp = fabs(x) * fabs(x) - fabs(y) * fabs(y) + env->juliax;
		y = 2 * fabs(x) * fabs(y) + env->juliay;
		x = tmp;
		iter--;
	}
	return (env->iter - iter);
}
