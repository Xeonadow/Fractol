/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 17:34:52 by lpousse           #+#    #+#             */
/*   Updated: 2016/12/08 13:34:32 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <stdlib.h>
# include <math.h>
# include <limits.h>
# include <mlx.h>
# include <errno.h>
# include "libft.h"
# include "keys.h"

# define WINX 1300
# define WINY 1300
# define ZOOM 1.1
# define MOVE 50
# define ITER 2

typedef struct	s_imgdata
{
	char	*img;
	int		bppixel;
	int		size_line;
	int		endian;
	int		x;
	int		y;
}				t_imgdata;

typedef struct	s_env
{
	int				fractal;
	void			*mlx;
	void			*win;
	double			winx;
	double			winy;
	double			zoom;
	int				iter;
	long			x;
	long			y;
	int				dragndrop;
	int				xdnd;
	int				ydnd;
	double			juliax;
	double			juliay;
	int				pause;
	void			*img;
	t_imgdata		imgdata;
	unsigned int	bkg;
	unsigned int	col1;
	unsigned int	col2;
	unsigned int	*colors;
}				t_env;

void			init_imgdata(void *img, t_env *env);
void			clear_img(t_imgdata *img);
void			put_pix_img(t_imgdata *img, int x, int y, unsigned int color);

int				draw(t_env *env);
void			colors(t_env *env);
void			iteration_control(int key, t_env *env);
void			move_control(int key, t_env *env);
void			fractal_control(int key, t_env *env);
void			color_control(int key, t_env *env);
void			zoom_control(int key, t_env *env);
void			reset(t_env *env);

int				mandelbrot(int i, int j, int iter, t_env *env);
int				julia(int i, int j, int iter, t_env *env);
int				burning_ship(int i, int j, int iter, t_env *env);
int				burning_julia(int i, int j, int iter, t_env *env);

int				expose_hook(t_env *env);
int				key_hook(int keycode, t_env *env);
int				mouse_hook(int button, int x, int y, t_env *env);
int				left_click_hook(int button, int x, int y, t_env *env);
int				pointer_hook(int x, int y, t_env *env);

void			error(int errcode);

#endif
