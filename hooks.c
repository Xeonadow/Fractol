/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 19:15:48 by lpousse           #+#    #+#             */
/*   Updated: 2016/12/08 13:34:35 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		expose_hook(t_env *env)
{
	char	*str;

	mlx_clear_window(env->mlx, env->win);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	ft_asprintf(&str, "iterations number: %d\n", env->iter);
	mlx_string_put(env->mlx, env->win, 10, 10, 0xFFFFFF, str);
	ft_strdel(&str);
	if (env->fractal == 1)
		mlx_string_put(env->mlx, env->win, env->winx - 200, 10,
						0xFFFFFF, "fractal: mandelbrot");
	else if (env->fractal == 2)
		mlx_string_put(env->mlx, env->win, env->winx - 150, 10,
						0xFFFFFF, "fractal: julia");
	else if (env->fractal == 3)
		mlx_string_put(env->mlx, env->win, env->winx - 220, 10,
						0xFFFFFF, "fractal: burning_ship");
	else if (env->fractal == 4)
		mlx_string_put(env->mlx, env->win, env->winx - 230, 10,
						0xFFFFFF, "fractal: burning_julia");
	return (0);
}

int		key_hook(int key, t_env *env)
{
	if (key == KBD_ESC)
		exit(0);
	else if (key == PAD_1 || key == PAD_2 || key == PAD_3
				|| key == PAD_4 || key == PAD_5 || key == PAD_6)
		color_control(key, env);
	else if (key == KBD_1 || key == KBD_2 || key == KBD_3
			|| key == KBD_4 || key == KBD_SPACE)
		fractal_control(key, env);
	else if (key == PAD_PLUS || key == PAD_MINUS)
		zoom_control(key, env);
	else if (key == KBD_UP || key == KBD_DOWN
				|| key == KBD_LEFT || key == KBD_RIGHT)
		move_control(key, env);
	else if (key == PAD_STAR || key == PAD_SLASH)
		iteration_control(key, env);
	else if (key == PAD_EQUAL)
	{
		reset(env);
		draw(env);
	}
	return (0);
}

int		mouse_hook(int button, int x, int y, t_env *env)
{
	double	zoom;

	if (button == MOUSE_LEFT
		&& x >= 0 && x < env->winx && y >= 0 && y < env->winy)
	{
		env->dragndrop = 1;
		env->xdnd = x;
		env->ydnd = y;
	}
	if ((button == WHEEL_UP || button == WHEEL_DOWN)
		&& x >= 0 && x < env->winx && y >= 0 && y < env->winy)
	{
		zoom = button == WHEEL_DOWN ? ZOOM : 1 / ZOOM;
		env->zoom *= zoom;
		env->x = env->x / zoom + (x - env->winx / 2) * (1 / zoom - 1);
		env->y = env->y / zoom + (y - env->winy / 2) * (1 / zoom - 1);
		draw(env);
	}
	return (0);
}

int		left_click_hook(int button, int x, int y, t_env *env)
{
	if (button == MOUSE_LEFT)
		env->dragndrop = 0;
	x = 0;
	y = 0;
	return (0);
}

int		pointer_hook(int x, int y, t_env *env)
{
	double	xj;
	double	yj;
	int		redraw;

	redraw = 0;
	if (env->dragndrop == 1)
	{
		env->x -= x - env->xdnd;
		env->y -= y - env->ydnd;
		env->xdnd = x;
		env->ydnd = y;
		redraw = 1;
	}
	if ((env->fractal == 2 || env->fractal == 4) && x >= 0 && y >= 0
			&& x < env->winx && y < env->winy && env->pause == 0)
	{
		xj = (x - env->winx / 2) * 4 / env->winx;
		yj = (y - env->winy / 2) * 4 / env->winx;
		env->juliax = xj;
		env->juliay = yj;
		redraw = 1;
	}
	if (redraw == 1)
		draw(env);
	return (0);
}
