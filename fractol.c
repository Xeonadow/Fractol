/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 17:35:04 by lpousse           #+#    #+#             */
/*   Updated: 2016/12/07 22:54:50 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		error(int errcode)
{
	if (errcode > 0)
		perror("ERROR");
	else
		ft_putendl("ERROR");
	exit(0);
}

static int	parsing(int ac, char **av)
{
	if (ac == 2)
	{
		if (ft_strcmp(av[1], "mandelbrot") == 0)
			return (1);
		else if (ft_strcmp(av[1], "julia") == 0)
			return (2);
		else if (ft_strcmp(av[1], "burning_ship") == 0)
			return (3);
		else if (ft_strcmp(av[1], "burning_julia") == 0)
			return (4);
	}
	ft_putendl("Usage : ./fractol fractal_name\n");
	ft_putendl("Availible fractals:\n    - mandelbrot\n    - julia");
	ft_putendl("    - burning_ship\n    - burning_julia");
	return (0);
}

void		reset(t_env *env)
{
	env->zoom = 1;
	env->iter = 20;
	env->x = 0;
	env->y = 0;
	env->juliax = 0.28;
	env->juliay = 0.008;
	env->pause = 0;
	env->dragndrop = 0;
	env->bkg = 0x000000;
	env->col1 = 0x303030;
	env->col2 = 0xFFFFFF;
	env->colors = NULL;
	colors(env);
}

void		print_instructions(void)
{
	ft_putendl("\n\nControls:\n");
	ft_putendl(" Arrows : move");
	ft_putendl(" Numpad(=) : reset\n Numpad(+) / Numpad(-) : zoom");
	ft_putendl(" Numpad(*) / Numpad(/) : iterations number");
	ft_putendl(" Numpad(1, 2, 3, 4, 5, 6) : change colors");
	ft_putendl(" Space : pause julia parameter modification");
	ft_putendl(" Mouse left click (hold) : move");
	ft_putendl(" Mouse wheel : zoom relative to cursor position");
	ft_putendl(" 1 : mandelbrot\n 2 : julia");
	ft_putendl(" 3 : burning_ship\n 4 : burning_julia");
}

int			main(int ac, char **av)
{
	t_env	env;

	env.fractal = parsing(ac, av);
	if (env.fractal == 0)
		return (-1);
	env.winx = WINX;
	env.winy = WINY;
	reset(&env);
	if (!(env.mlx = mlx_init()) || env.winx <= 0 || env.winy <= 0
		|| !(env.win = mlx_new_window(env.mlx, env.winx, env.winy, "fractol")))
		error(-1);
	env.img = mlx_new_image(env.mlx, env.winx, env.winy);
	init_imgdata(env.img, &env);
	mlx_expose_hook(env.win, expose_hook, &env);
	mlx_hook(env.win, 2, 1, key_hook, &env);
	mlx_hook(env.win, 6, (1L << 6), pointer_hook, &env);
	mlx_hook(env.win, 5, (1L << 3), left_click_hook, &env);
	mlx_mouse_hook(env.win, mouse_hook, &env);
	draw(&env);
	print_instructions();
	mlx_loop(env.mlx);
	return (0);
}
